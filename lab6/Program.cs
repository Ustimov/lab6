﻿using System;
using InterSystems.Data.CacheClient;
using User;

namespace lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "Server = localhost;"
                                    + " Port = 1972;"
                                    + " Namespace = User;"
                                    + " Password = Qwerty123!;"
                                    + " User ID = _system;";

            var cacheConnection = new CacheConnection { ConnectionString = connectionString };
            cacheConnection.Open();

            var catalogId = "2";
            Catalog catalog = null;
            if (Catalog.ExistsId(cacheConnection, catalogId) ?? false)
            {
                Console.WriteLine($"Catalog with id={catalogId} exists\n");
                catalog = Catalog.OpenId(cacheConnection, catalogId);
            }
            else
            {
                Console.WriteLine($"Catalog with id={catalogId} not found\n");
            }

            if (catalog != null)
            {
                var file = new MyFile(cacheConnection)
                {
                    description = "Created from C#",
                    name = "C#",
                    size = 1,
                    type = "Text",
                    cat = catalog,
                };
                file.Save();
                Console.WriteLine($"Created a new file {file.name}\n");
            }

            var userId = "1";
            if (MyUser.ExistsId(cacheConnection, userId) ?? false)
            {
                var user = MyUser.OpenId(cacheConnection, userId);
                Console.WriteLine($"Fullname for user with id={userId} (method without params):\n" +
                                  $"{user.getFullName()}");
                var param = "param";
                Console.WriteLine($"Method with params:\n* passed: {param}\n" +
                                  $"* returned: {user.testMethod(param)}");
            }
            else
            {
                Console.WriteLine($"User with id={userId} not found");
            }
        }
    }
}
